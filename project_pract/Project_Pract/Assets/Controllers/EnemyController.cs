﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyController : MonoBehaviour
{
    public float lookRadius = 10f;
    public float hitRadius = 1f;
    Animator _animator;
    Transform target;
    NavMeshAgent agent;
    public AudioClip Swing;
    public AudioSource Enemy;
    float _timeLeft = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        
        float distance = Vector3.Distance(target.position, transform.position);

        if (distance <= lookRadius)
        {
            agent.SetDestination(target.position);

            if (distance <= agent.stoppingDistance)
            {
                //Make attack target script
                FaceTarget();
                _timeLeft -= Time.deltaTime;
            }
        if (distance <= hitRadius)

                if (_timeLeft<= 0.0f)
            {
                //attack
                _animator.SetBool("Attack", true);

            }
        if(distance > hitRadius)
            {
             _animator.SetBool("Attack", false);
             _timeLeft = 2.0f;
             
            }
        }
    }

    void AttackEnd()
    {
        //damage player
        PlayerHealth.instance.TakeDamage(20);
        Enemy.PlayOneShot(Swing);
        
        
    }
    void FaceTarget ()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);

    }

    void OnDrawGizmosSelected ()

    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
        
        
}
