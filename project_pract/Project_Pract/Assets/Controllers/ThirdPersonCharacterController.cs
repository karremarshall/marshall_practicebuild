﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCharacterController : MonoBehaviour
{
    public float speed;
    public Animator anim;
    public KeyCode attack;
    public KeyCode run;
    public KeyCode dodge;
    public KeyCode guard;
    public KeyCode runL;
    public KeyCode AxeThrow;
    public KeyCode runF;
    public KeyCode runB;
    bool canDodge = true;


    public RaycastTesting rayCast;
    // Update is called once per frame
    void Update()
    {

        PlayerMovement();
        if (Input.GetKeyDown(attack))

            anim.SetTrigger("PlayerAttack");

        if (Input.GetKey(run))
            anim.SetBool("PlayerRun", true);
        else anim.SetBool("PlayerRun", false);

        if (Input.GetKeyDown(dodge) && canDodge)
        {
            canDodge = false;
            anim.SetTrigger("PlayerDodge");
            speed = 10f;
            Invoke("dodgeFinish", 1f);
        }
       
        
            
        if (Input.GetKey(guard))
            anim.SetTrigger("PlayerGuard");

        if (Input.GetKey(runL))
            anim.SetBool("PlayerRunL", true);
        else anim.SetBool("PlayerRunL", false);

        if (Input.GetKeyDown(AxeThrow))
            anim.SetTrigger("AxeThrow");

        if (Input.GetKey(runF))
            anim.SetBool("RunningF", true);
        else anim.SetBool("RunningF", false);

        if (Input.GetKey(runB))
            anim.SetBool("RunningB", true);
        else anim.SetBool("RunningB", false);

    }

    void dodgeFinish()
    {
        canDodge = true;
        speed = 5f;
    }
    void PlayerMovement()
    {

        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 playerMovement = new Vector3(hor, 0f, ver) * speed * Time.deltaTime;
        transform.Translate(playerMovement, Space.Self);
    }
    public void attackEvent()
    {
        rayCast.DoAttack();
    }
}
