﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastTesting : MonoBehaviour
{
    public float maxRayDistance = 25;
    public float attackDamage;
    public Camera cam;
    public float coolDown;

    private float attackTimer;

    void FixedUpdate()
    {
        Ray ray = new Ray(transform.position, Vector3.forward);
        RaycastHit hit;

        Debug.DrawLine(transform.position, transform.position + Vector3.forward * maxRayDistance, Color.red);

        if (Physics.Raycast(ray, out hit, maxRayDistance))
        {
            Debug.DrawLine(hit.point, hit.point + Vector3.up * 5, Color.green);
        }

    }

    private void Update()
    {
        attackTimer += Time.deltaTime;
        if (Input.GetMouseButtonUp(0) && attackTimer >= coolDown)
        {
            attackTimer = 0f;
            //DoAttack();
        }
    }

    public void DoAttack()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, maxRayDistance))
        {
            if(hit.collider.tag == "Enemy")
            {
                EnemyHealth eHealth = hit.collider.GetComponent<EnemyHealth>();
                eHealth.TakeDamage(attackDamage);
             
            }

            if (hit.collider.tag == "EnemyK")
            {
                
                EnemyHealthK eHealthK = hit.collider.GetComponent<EnemyHealthK>();
                eHealthK.TakeDamage(attackDamage);
            }

            if (hit.collider.tag == "EnemyM")
            {

                EnemyHealthM eHealthM = hit.collider.GetComponent<EnemyHealthM>();
                eHealthM.TakeDamage(attackDamage);
            }
        }
    }


}
