﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class SlayCounter2 : MonoBehaviour

    
{   public static int scoreValue = 0;
    Text Score;

// Start is called before the first frame update
void Start()
    {
        Score = GetComponent<Text> ();
        scoreValue = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Score.text = "Slain: " + scoreValue;
        if (scoreValue == 3)
        {
            SceneManager.LoadScene("Victory");
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
