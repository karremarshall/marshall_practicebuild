﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialAttack : MonoBehaviour
{
    public float maxRayDistance = 25;
    public float attackDamage;
    public Camera cam;
    public float coolDown = 15f;
    public Animator anim;
    public Image CooldownBar;

    private float attackTimer;

    private void Start()
    {
        attackTimer = coolDown;
    }

    void FixedUpdate()
    {
        Ray ray = new Ray(transform.position, Vector3.forward);
        RaycastHit hit;

        Debug.DrawLine(transform.position, transform.position + Vector3.forward * maxRayDistance, Color.magenta);

        if (Physics.Raycast(ray, out hit, maxRayDistance))
        {
            Debug.DrawLine(hit.point, hit.point + Vector3.up * 5, Color.blue);
        }

    }

    private void Update()
    {
        attackTimer += Time.deltaTime;

        CooldownBar.fillAmount = attackTimer / coolDown;
        if (Input.GetKeyDown("space") && attackTimer >= coolDown)
        {
            
            attackTimer = 0f;
            DoAttack();
            anim.SetTrigger("AxeThrow");
        }
    }

    private void DoAttack()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, maxRayDistance))
        {
            if(hit.collider.tag == "Enemy")
            {
                EnemyHealth eHealth = hit.collider.GetComponent<EnemyHealth>();
                eHealth.TakeDamage(attackDamage);
            }
        }
    }


}
