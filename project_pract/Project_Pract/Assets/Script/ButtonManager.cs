﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
 
   public void NewGameBtn(string Marshall_final2)
    {
        SceneManager.LoadScene(Marshall_final2);
    }

    public void TryAgainBtn(string Marshall_final2)
    {
        SceneManager.LoadScene(Marshall_final2);
    }

    public void ExitGameBtn()
    {
        Application.Quit();
    }
}
