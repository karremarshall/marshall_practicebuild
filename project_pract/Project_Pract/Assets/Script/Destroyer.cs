﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    public float lifetime = 1f;
    // Update is called once per frame
    void Update()
    {
        if (lifetime > 0)
        {
            lifetime -= Time.deltaTime;
                if (lifetime <= 0)
            {
                destruction();
            }
        }
    }
    void destruction()
    {
        Destroy(this.gameObject);
    }
}
