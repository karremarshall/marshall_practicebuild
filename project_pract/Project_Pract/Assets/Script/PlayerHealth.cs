﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{

    #region Singleton
    public static PlayerHealth instance;

    void Awake()
    {
        instance = this;

    }
    #endregion

    public GameObject player;
    public Image HealthBar;
    public float maxHealth = 100f;
    private float health;
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        //HealthBar = GetComponent<Image>();
        health = maxHealth;

    }

    // Update is called once per frame
    void Update()
    {
        //HealthBar.fillAmount = health / maxHealth;


    }
    public void TakeDamage(float amnt)
    {

        health -= amnt;

        HealthBar.fillAmount = health / maxHealth;

        if (health <= 0)
        {
            print("NOOOOO");
            Destroy(gameObject);
            Application.LoadLevel("Game Over");
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;


        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        print("I took a hit");
        
    }

}
