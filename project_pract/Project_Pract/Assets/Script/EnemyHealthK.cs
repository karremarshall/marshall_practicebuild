﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthK : MonoBehaviour
{
  
    public float Health = 400;

    private float currentHealth;

    public Image HealthBar;

    private void Start()
    {
        currentHealth = Health;
    }


    public void TakeDamage(float amnt)
    {

        currentHealth -= amnt;

        HealthBar.fillAmount = currentHealth / Health;

        if (currentHealth <= 0)
        {
            print("Tango Down");
            Destroy(gameObject);
            SlayCounter.scoreValue += 1;
        }

        print("Enemy took a hit");
    }
}
