﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateEnemy : MonoBehaviour
{

    public GameObject theEnemy;
    public int xPos;
    public int zPos;
    public int enemyCount;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(EnemyDrop());

    }


    IEnumerator EnemyDrop()
        {
            while (enemyCount < 15)
            {
                xPos = Random.Range(-87, 75);
                zPos = Random.Range(-80, 54);
                Instantiate(theEnemy, new Vector3(xPos, 0.5f, zPos), Quaternion.identity);
                yield return new WaitForSeconds(8);
                enemyCount += 1;
            }
        }
    


}
